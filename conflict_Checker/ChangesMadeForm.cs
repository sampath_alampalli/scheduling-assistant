﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace conflict_Checker
{
    public partial class ChangesMadeForm : Form
    {
        public ChangesMadeForm()
        {
            InitializeComponent();
            updateTextBox.Enabled = false;
            updateTextBox.BackColor = Color.White;
            updateTextBox.Text = "I am changed";

        }

        private void updatedValuesTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTextBox.Enabled = false;
            updateTextBox.BackColor = Color.White;
            updateTextBox.Text = "I am changed";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
